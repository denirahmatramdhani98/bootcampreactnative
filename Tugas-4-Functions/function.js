// Soal Nomor 1
console.log("=====Soal 1=====")
function teriak(){
    return "Halo Sanbers!"
}
console.log(teriak())

// Soal Nomor 2
console.log()
console.log("=====Soal 2=====")
function kalikan(nilai1,nilai2){
    return nilai1*nilai2
}
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali)

// Soal Nomor 3
console.log()
console.log("=====Soal 3=====")
function introduce(name, age, address, hobby){
    return "Nama saya "+name+", umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!"
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)