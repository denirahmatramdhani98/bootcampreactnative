// Soal Nomor 1
console.log("=====Soal 1=====")
var angka = 2
console.log("LOOPING PERTAMA")
while(angka<=20){
    console.log(angka + " - I Love Coding")
    angka += 2;
}
console.log("LOOPING KEDUA")
while(angka>=4){
    angka -= 2;
    console.log(angka + " - I will become a mobile developer")
}

// Soal Nomor 2
console.log()
console.log("=====Soal 2=====")
for (var a=1;a<=20;a++){
    if(a%3===0 && a%2!==0){
        console.log(a+" - I Love Coding")
    }else if(a%2===0){
        console.log(a+" - Berkualitas")
    }else{
        console.log(a+" - Santai")
    }
}

// Soal Nomor 3
console.log()
console.log("=====Soal 3=====")
for (var a=1;a<=4;a++){
    var output = ""
    for(var b=1;b<=8;b++){
        output += "#"
    }
    console.log(output)
}

// Soal Nomor 4
console.log()
console.log("=====Soal 4=====")
var output1 = ""
for (var a=0;a<7;a++){
    output1 += "#"
    console.log(output1)
}

// Soal Nomor 5
console.log()
console.log("=====Soal 5=====")
for (var a=1;a<=8;a++){
    var output2 = ""
    for(var b=1;b<=8;b++){
        if((a+b)%2===0){
            output2 += " "
        }else{
            output2 += "#"
        }
    }
    console.log(output2)
}