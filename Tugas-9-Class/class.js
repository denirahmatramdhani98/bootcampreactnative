class Animal {
    constructor(name) {
        this._name = name;
        this._leg = 4;
        this._cold_blooded = false;
    }
    get name() {
        return this._name;
    }
    set name(x) {
        this._name = x;
    }
    get legs() {
        return this._leg;
    }
    set legs(x) {
        this._leg = x;
    }
    get cold_blooded() {
        return this._cold_blooded;
    }
    set cold_blooded(x) {
        this._cold_blooded = x;
    }
}

class Frog extends Animal {
    legs = 2;

    Jump = () => {
        console.log('hop hop');
    }
}

class Ape extends Animal {
    Yell = () => {
        console.log('Auuuooo');
    }
}

let sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

let sungokong = new Ape("kera sakti")
sungokong.Yell() // "Auooo"

let kodok = new Frog("buduk")
kodok.Jump() // "hop hop" 

class Clock {
    constructor({ template }) {
        this.template = template;
    }
    render = () => {
        let date = new Date();
        let hours = date.getHours();
        if (hours < 10) {
            hours = '0' + hours;
        }

        let mins = date.getMinutes();
        if (mins < 10) {
            mins = '0' + mins;
        }

        let secs = date.getSeconds();
        if (secs < 10) {
            secs = '0' + secs;
        }

        let output = this.template.replace('h', hours).replace('m', mins).replace('s', secs);
        console.log(output);
    }
    stop = () => {
        clearInterval();
    }

    start = () => {
        this.render();
        this.timer = setInterval(() => {
            this.render()
        }, 1000);
    }
}

let Clocks = new Clock({template : 'h:m:s'});
Clocks.start();
