// Soal Nomor 1
console.log("=====Soal 1=====")
let golden = () => {
    console.log('this is golden');
}
golden();

// Soal Nomor 2
console.log()
console.log("=====Soal 2=====")
let newFunction = (namaDepan, namaBelakang) => {
    return {
        namaDepan,
        namaBelakang,
        fullName() {
            console.log(namaDepan + ' ' + namaBelakang);
            return;
        }
    }
}
newFunction('enggar', 'joharnovta').fullName();

// Soal Nomor 3
console.log()
console.log("=====Soal 3=====")
let newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
let {
    firstName,
    lastName,
    destination,
    occupation,
    spell
} = newObject;
console.log(firstName, lastName, destination, occupation, spell);

// Soal Nomor 4
console.log()
console.log("=====Soal 4=====")
let west = ["Will", "Chris", "Sam", "Holly"];
let east = ["Gill", "Brian", "Noel", "Maggie"];
let combined = [...west,...east];
console.log(combined);

// Soal Nomor 5
console.log()
console.log("=====Soal 5=====")
let planet = "earth"
let view = "glass"
var before = `Lorem ${view} dolor sit amet, ` +  
    `consectetur adipiscing elit, ${planet} do eiusmod tempor ` +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 console.log(before);
