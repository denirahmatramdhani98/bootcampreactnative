let ReadBooksPromise = (time, book) => {
    console.log(`saya mulai membaca ${book.name}`);
    return new Promise(function (resolve, reject){
        setTimeout(() => {
            let sisaWaktu = time - book.timeSpent;
            if(sisaWaktu >= 0){
                console.log(`saya sudah selesai memnaca ${book.name}, sisa waktu saya ${sisaWaktu}`);
                resolve(sisaWaktu);
            } else {
                console.log(`saya sudah tidak punya waktu membaca ${book.name}`);
                reject(sisaWaktu);
            }
        }, book.timeSpent);
    });
}

module.exports = ReadBooksPromise;
