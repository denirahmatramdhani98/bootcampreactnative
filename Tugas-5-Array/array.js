// Soal Nomor 1
console.log("=====Soal 1=====")
function range(startNum, finishNum){
    var simpan = []
    if(startNum>finishNum){
        while(finishNum<=startNum){
            simpan.push(finishNum)
            simpan.sort(function (value1, value2) { return value2 - value1 } )
            finishNum++
        }
    }else if(startNum<finishNum){
        while(startNum<=finishNum){
            simpan.push(startNum)
            startNum++
        }
        
    }else{
        simpan = -1
    }
    return simpan
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal Nomor 2
console.log()
console.log("=====Soal 2=====")
// function rangeWithStep(startNum, finishNum, step){
//     var simpan = []
//     if(startNum>finishNum){
//         while(startNum>=finishNum){
//             simpan.push(startNum)
//             simpan.sort(function (value1, value2) { return value2 - value1 } )
//             startNum -= step
//         }
//     }else if(startNum<finishNum){
//         while(startNum<=finishNum){
//             simpan.push(startNum)
//             startNum+=step
//         }
        
//     }else{
//         simpan = -1
//     }
//     return simpan
// }
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal Nomor 3
console.log()
console.log("=====Soal 3=====")
function Sum(startNum, finishNum, step) {
    let result = 0;
    if (!step) {
        step = 1;
    }
    if (!startNum && finishNum) {
        return finishNum;
    } else if (startNum && !finishNum) {
        return startNum;
    } else if (startNum < finishNum) {
        for (let position = startNum; position <= finishNum;) {
            result += position;
            position += step;
        }
    } else if (startNum > finishNum) {
        for (let position = startNum; position >= finishNum;) {
            result += position
            position -= step;
        }
    }
    return result;
}
console.log(Sum(1, 10)); // 55
console.log(Sum(5, 50, 2)); // 621
console.log(Sum(15, 10)); // 75
console.log(Sum(20, 10, 2)); // 90
console.log(Sum(1)); // 1
console.log(Sum()); // 0 

// Soal Nomor 4
console.log()
console.log("=====Soal 4=====")
function DataHandling(data) {
    if (!data) {
        return 'No Data';
    }

    let result = '';
    if (data.length > 0) {
        for (let position = 0; position < data.length; position++) {
            // console.log(data[position].length);
            result += 'Nomor ID: ' + data[position][0] + '\n' +
                'Nama Lengkap: ' + data[position][1] + '\n' +
                'TTL:' + data[position][2] + '\n' +
                'Hobi:' + data[position][3] + '\n';
            result += '\n';
        }
        return result;
    } else {
        return 'No Data';
    }
}
let input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];
console.log(DataHandling(input));

// Soal Nomor 5
console.log()
console.log("=====Soal 5=====")
function BalikKata(data) {
    let result = '';
    for (let pos = data.length - 1; pos >= 0; pos--) {
        result += data[pos];
    }
    return result;
}
console.log(BalikKata("Kasur Rusak"));
console.log(BalikKata("SanberCode"));
console.log(BalikKata("Haji Ijah"));
console.log(BalikKata("racecar"));
console.log(BalikKata("I am Sanbers"));

// Soal Nomor 6
console.log()
console.log("=====Soal 6=====")
function DataHandling2(data) {
    data[4] = 'Pria';
    data.push('SMA Internasional Metro');
    data[1] += 'Elsharawy';
    let newProvinsi = 'Provinsi ';
    let oldProvinsi = data[2];
    data[2] = newProvinsi + oldProvinsi;
    console.log(data);
    let dates = data[3].split('/');
    let month = parseInt(dates[1]);
    switch (month) {
        case 1:
            console.log('Januari');
            break;
        case 2:
            console.log('Februari');
            break;
        case 3:
            console.log('Maret');
            break;
        case 4:
            console.log('April');
            break;
        case 5:
            console.log('Mei');
            break;
        case 6:
            console.log('Juni');
            break;
        case 7:
            console.log('Juli');
            break;
        case 8:
            console.log('Agustus');
            break;
        case 9:
            console.log('Septembe');
            break;
        case 10:
            console.log('Oktober');
            break;
        case 11:
            console.log('November');
            break;
        case 12:
            console.log('Desember');
            break;
        default:
            console.log('Bulan tidak dapat dikenali');

    }
    for (let i = 0; i < dates.length; i++) {
        dates[i] = dates[i] * 1;
    }
    dates = dates.sort();
    for (let i = 0; i < dates.length; i++) {
        dates[i] = dates[i].toString();
        if (dates[i].length < 2) {
            dates[i] = '0' + dates[i];
        }
    }
    console.log(dates);
    let newArrayDates = [];
    newArrayDates.push(dates[1]);
    newArrayDates.push(dates[2]);
    newArrayDates.push(dates[0]);
    let newDate = newArrayDates.join('-');
    console.log(newDate);
    let arrayName = data[1].split("");
    arrayName = arrayName.slice(0,14);
    let newName = arrayName.join("");
    console.log(newName);


}
let dataInput = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
DataHandling2(dataInput);

