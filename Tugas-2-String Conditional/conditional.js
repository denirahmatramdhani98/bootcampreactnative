// Soal Nomor 1
console.log("=====Soal 1=====")
var nama = "Agus"
var peran = ""

if (nama === "" || nama === " "){
    console.log("Nama harus diisi!")
} else{
    if(peran === "Penyihir"){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo Penyihir"+nama+", kamu dapat melihat siapa yang menjadi werewolf!")
    }else if(peran === "Guard"){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo Penyihir"+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    }else if(peran === "Werewolf"){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo Penyihir"+nama+", Kamu akan memakan mangsa setiap malam!")
    }else{
        console.log("Halo "+ nama+", Pilih peranmu untuk memulai game!")
    }
}

// Soal Nomor 2
console.log()
console.log("=====Soal 2=====")
var hari = 1; 
var bulan = 15; 
var tahun = 1900;
var strbulan
switch (bulan){
    case 1:
        strbulan = "Januari"
        break
    case 2:
        strbulan = "Februari"
        break
    case 3:
        strbulan = "Maret"
        break
    case 4:
        strbulan = "April"
        break
    case 5:
        strbulan = "Mei"
        break
    case 6:
        strbulan = "Juni"
        break
    case 7:
        strbulan = "Juli"
        break
    case 8:
        strbulan = "Agustus"
        break
    case 9:
        strbulan = "September"
        break
    case 10:
        strbulan = "Oktober"
        break
    case 11:
        strbulan = "November"
        break
    case 12:
        strbulan = "Desember"
        break
    default:
        bulan = bulan
        break
}

// Cara Conditional 1
if((hari>31 || hari<1) && (tahun>2200 || tahun<1900) && (bulan>12 || bulan<1)){
    console.log("Masukkan tanggal dengan benar")
}else if(hari>31 || hari<1){
    console.log("Masukkan hari diantara 1-31")
}else{
    if(tahun>2200 || tahun<1900){
        console.log("Masukkan tahun diantara 1900-2200")
    }else{
        if(bulan>12 || bulan<1){
            console.log("Masukkan bulan diantara 1-12")
        }else{
            console.log(hari+" " + strbulan+" " + tahun)
        }
    }
}

// Cara Conditional 2
// if((hari>31 || hari<1) || (tahun>2200 || tahun<1900) || (bulan>12 || bulan<1)){
//     console.log("Masukkan tanggal dengan benar")
// }else{
//     console.log(hari+" " + strbulan+" " + tahun)
// }